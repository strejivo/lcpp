#include "knownExpressions.h"

namespace knownExpressions {
	Lambda* True() {
		return new Lambda(
		  "t",
		  Application({
						new Lambda(
						  "f",
						  Application({
										new Variable("t")
									  })
						)
					  }),
		  "TRUE"
		);
	}

	Lambda* False() {
		return new Lambda(
		  "t",
		  Application({
						new Lambda(
						  "f",
						  Application({
										new Variable("f")
									  })
						)
					  }),
		  "FALSE"
		);
	}

	Lambda* Not() {
		return new Lambda(
		  "x",
		  Application({
						new Lambda(
						  "t",
						  Application({
										new Lambda(
										  "f",
										  Application({
														new Variable("x"),
														new Variable("f"),
														new Variable("t")
													  })
										)
									  })
						)
					  }),
		  "NOT"
		);
	}

	Lambda* Y() {
		// (λf. (λx. f (xx)) (λx. f (xx)))
		return new Lambda(
		  "f",
		  Application({
						new Lambda(
						  "x",
						  Application({
										new Variable("f"),
										new Application({
														  new Variable("x"),
														  new Variable("x")
														})
									  })
						),
						new Lambda(
						  "x",
						  Application({
										new Variable("f"),
										new Application({
														  new Variable("x"),
														  new Variable("x")
														})
									  })
						)
					  }),
		  "Y"
		);
	}

	Lambda* Zero() {
		// λn.n F NOT F
		return new Lambda(
		  "n",
		  Application({
						new Variable("n"),
						False(),
						Not(),
						False()
					  }),
		  "isZero"
		);
	}

	Lambda* MinusOne() {
		//  (λx. λs. λz. x (λf.λg.g (f s)) (λg.z) (λm.m))
		return new Lambda(
		  "x",
		  Application({
						new Lambda(
						  "s",
						  Application({
										new Lambda(
										  "z",
										  Application({
														new Variable("x"),
														new Lambda(
														  "f",
														  Application({
																		new Lambda(
																		  "g",
																		  Application({
																						new Variable("g"),
																						new Application({
																										  new Variable(
																											"f"),
																										  new Variable(
																											"s")
																										})
																					  })
																		)
																	  })
														),
														new Lambda(
														  "g",
														  Application({
																		new Variable("z")
																	  })
														),
														new Lambda(
														  "m",
														  Application({
																		new Variable("m")
																	  })
														)
													  })
										)
									  })
						)
					  }),
		  "minus_one"
		);
	}

	Lambda* Multiply() {
		//  (λx. λy. λz. x (y z))
		return new Lambda(
		  "x",
		  Application({
						new Lambda(
						  "y",
						  Application({
										new Lambda(
										  "s",
										  Application({
														new Variable("x"),
														new Application({
																		  new Variable("y"),
																		  new Variable("s")
																		})
													  })
										)
									  })
						)
					  }),
		  "multiply"
		);
	}

	Lambda* Plus() {
		return new Lambda(
		  "m",
		  Application({
						new Lambda(
						  "n",
						  Application({
										new Lambda(
										  "s",
										  Application({
														new Lambda(
														  "z",
														  Application({
																		new Variable("m"),
																		new Variable("s"),
																		new Application({
																						  new Variable("n"),
																						  new Variable("s"),
																						  new Variable("z")
																						})
																	  })
														)
													  })
										)
									  })
						)
					  }),
		  "plus"
		);
	}
}