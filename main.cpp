#include <iostream>
#include "lambda_calc.h"

int main() {
	Expression * R =
			new Lambda(
				"f",
				Application({
					new Lambda(
						"n",
						Application({
							knownExpressions::Zero(),
							new Variable("n"),
							createChurchNumber(1),
							new Application({
								knownExpressions::Multiply(),
								new Variable("n"),
								new Application({
									new Variable("f"),
									new Application({
										knownExpressions::MinusOne(),
										new Variable("n")
									})
								})
							})
						})
					)
				}),
				"R"
			);

	Expression * v = new Application({
					 	knownExpressions::Y(),
					 	R,
					 	createChurchNumber(6)
					 });
	//std::cout << *v << "\n----------------------------------------\n";
	while (v->tryApply()) {
		//std::cout << *v << "\n----------------------------------------\n";
	}
	std::cout << *v << std::endl;
	delete v;
	return 0;
}
