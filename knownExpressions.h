#ifndef LC_KNOWNEXPRESSIONS_H
#define LC_KNOWNEXPRESSIONS_H

#include <map>
#include <memory>
#include "lambda_calc.h"

namespace knownExpressions{
	Lambda* True();

	Lambda* False();

	Lambda* Not();

	Lambda* Y();

	Lambda* Zero();

	Lambda* MinusOne();

	Lambda* Multiply();

	Lambda* Plus();
}

#endif //LC_KNOWNEXPRESSIONS_H
