#include "lambda_calc.h"
#include <algorithm>
#include <utility>

VariableIdentifier createReplacementName(){
	// Temporary implementation
	static int replacementCounter = 0;
	return "rep" + std::to_string(replacementCounter++);
}

Application createNumberBase(unsigned int x) {
	if (x == 0) {
		return Application({new Variable("z")});
	} else {
		Application* inner = new Application({new Variable("s"), new Variable("z")});
		for (unsigned int i = 1; i < x; i++) {
			inner = new Application({new Variable("s"), inner});
		}
		Application ret(*inner);
		delete inner;
		return ret;
	}
}

Lambda * createChurchNumber(unsigned int x){
	return new Lambda(
		"s",
		Application({
			new Lambda(
				"z",
				createNumberBase(x)
			)
		}),
		std::to_string(x)
	);
}

std::ostream& operator<< (std::ostream& os, const Expression& ex) {
	ex.printOnStream(os);
	return os;
}

Expression::Expression(std::set<VariableIdentifier> freeVariables) : freeVariables(std::move(freeVariables)) {}

//-----------------------------------------------------------------------------------
// VARIABLE
//-----------------------------------------------------------------------------------

Variable::Variable(VariableIdentifier x) : Expression({x}), name(std::move(x)){}

Variable::Variable(const Variable & x) : Expression({x.freeVariables}), name(x.name){
}

Variable::~Variable() {}

Variable * Variable::clone() const {
	return new Variable(*this);
}

void Variable::printOnStream(std::ostream &os) const {
	os<<name;
}

void Variable::replace(const VariableIdentifier&, Expression *) {}

bool Variable::tryApply() {
	return false;
}

bool Variable::shouldReplace(const VariableIdentifier& x) const{
	return x == name;
}

//-----------------------------------------------------------------------------------
// APPLICATION
//-----------------------------------------------------------------------------------

std::set<VariableIdentifier> collectFreeVariables(const std::vector<Expression *>& expressions) {
	std::set<VariableIdentifier> ret;
	for (Expression* e : expressions) {
		ret.insert(e->freeVariables.begin(), e->freeVariables.end());
	}
	return ret;
}

Application::Application(std::vector<Expression *> expr) : Expression(collectFreeVariables(expr)), expressions(std::move(expr)) {
}

Application::Application(const Application & x) : Expression(x.freeVariables) {
	for (auto it : x.expressions){
		expressions.push_back(it->clone());
	}
}

Application::~Application() {
	for (auto it : expressions){
		delete it;
	}
}

Application * Application::clone() const {
	return new Application(*this);
}

void Application::printOnStream(std::ostream &os) const {
	if (expressions.size() > 1 ) os<<"(";
	for (auto it : expressions){
		it->printOnStream(os);
		if (it != expressions.back()) os<<" ";
	}
	if (expressions.size() > 1 ) os<<")";
}

void Application::replace(const VariableIdentifier& x, Expression* replacement) {
	if (freeVariables.erase(x) != 0) {
		freeVariables.insert(replacement->freeVariables.begin(), replacement->freeVariables.end());
		for (auto& it : expressions) {
			if (it->shouldReplace(x)) {
				delete it;
				it = replacement->clone();
			} else {
				it->replace(x, replacement);
			}
		}
	}
}

bool Application::tryApply() {
	for (size_t i = 0; i + 1 < expressions.size(); ++i){
		Lambda* lambda = dynamic_cast<Lambda *> (expressions[i]);
		if (lambda) {
			Expression * applicant = expressions[i+1];
			// do the replacement
			auto& result = lambda->apply(applicant);
			result.simplify_();
			// delete the old lambda and applicant from the expressions vector
			expressions.erase(expressions.begin()+i);
			expressions.erase(expressions.begin()+i);
			//insert all the result expressions
			size_t index = i;
			for (auto it : result.expressions){
				expressions.insert(expressions.begin()+index++, it);
			}
			result.expressions.clear();
			delete lambda;
			delete applicant;
			return true;
		}
		else {
			// try to do application inside a expression
			if (expressions[i]->tryApply()){
				simplify_();
				return true;
			}
		}
	}
	// try to do application on the last expression
	if (expressions.back()->tryApply()){
		simplify_();
		return true;
	}
	return false;
}

void Application::simplify_() {
	if (expressions.size() == 1){
		auto* application = dynamic_cast<Application *>(expressions.front());
		if (application){
			// in this application there is only other application, so we can take the expressions of the inner application and put them in this application
			expressions.clear();
			expressions.reserve(application->expressions.size());
			for (auto it : application->expressions){
				expressions.push_back(it);
			}
			application->expressions.clear();
			delete application;
		}
	} else {
		for (auto& expression : expressions) {
			auto* application = dynamic_cast<Application*>(expression);
			if (application && application->expressions.size() == 1) {
				// if the inner lambda has only one expression, replace the inner application with that expression
				expression = application->expressions.front();
				application->expressions.clear();
				delete application;
			}
		}
	}
}

//-----------------------------------------------------------------------------------
// LAMBDA
//-----------------------------------------------------------------------------------

std::set<VariableIdentifier> removeArg(std::set<VariableIdentifier> freeVariables, const VariableIdentifier& var) {
	freeVariables.erase(var);
	return freeVariables;
}

Lambda::Lambda(const VariableIdentifier &arg, const Application& body, std::string alias)
	: Expression(removeArg(body.freeVariables, arg)), arg(arg), body(body), alias(std::move(alias)) {}

Lambda::Lambda(const Lambda & x) : Expression(x.freeVariables), arg(x.arg), body(x.body){
	alias = x.alias;
}

Lambda * Lambda::clone() const {
	return new Lambda(*this);
}

void Lambda::printOnStream(std::ostream &os) const {
	if (!alias.empty()){
		os<<alias;
		return;
	}
	os << "(lambda " << arg << ".";
	body.printOnStream(os);
	os << ")";
}

void Lambda::replace(const VariableIdentifier& x, Expression *replacement) {
	if (freeVariables.erase(x) != 0) {
		freeVariables.insert(replacement->freeVariables.begin(), replacement->freeVariables.end());
		if (replacement->freeVariables.find(arg) != replacement->freeVariables.end()) {
			// there should be a alpha reduction
			VariableIdentifier newName = createReplacementName();
			Variable* varTmp = new Variable(newName);
			// alpha reduction
			body.replace(arg, varTmp);
			arg = newName;
			delete varTmp;
		}
		body.replace(x, replacement);
		alias = "";
	}
}

bool Lambda::tryApply() {
	return body.tryApply();
}

Application& Lambda::apply(Expression* applicant) {
	body.replace(arg, applicant);
	return body;
}
