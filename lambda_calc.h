#ifndef LC_PARSER_H
#define LC_PARSER_H

#include <iostream>
#include <ostream>
#include <string>
#include <vector>
#include <set>

using VariableIdentifier = std::string;

class Expression {
  public:
	Expression(std::set<VariableIdentifier>);
	virtual ~Expression() = default;
	virtual Expression * clone() const = 0;
	friend std::ostream &operator<<(std::ostream &os, const Expression &ex);
	virtual void printOnStream(std::ostream &os) const = 0;
	virtual void replace(const VariableIdentifier &, Expression *) = 0;
	virtual bool tryApply() = 0;
	virtual bool shouldReplace(const VariableIdentifier &) const { return false; }
	std::set<VariableIdentifier> freeVariables;
};

class Variable : public Expression {
  public:
	Variable(VariableIdentifier x);
	Variable(const Variable &);
	virtual ~Variable();
	Variable & operator = (const Variable &) = default;
	Variable * clone() const override;
	void printOnStream(std::ostream &os) const override;
	void replace(const VariableIdentifier &, Expression *replacement) override;
	bool tryApply() override;
	bool shouldReplace(const VariableIdentifier &) const override;
	VariableIdentifier name;
};

class Application : public Expression {
  public:
	Application(std::vector<Expression *> expressions);
	Application(const Application &);
	virtual ~Application();
	Application * clone() const override;
	void printOnStream(std::ostream &os) const override;
	void replace(const VariableIdentifier &, Expression *replacement) override;
	bool tryApply() override;
	std::vector<Expression *> expressions;
  private:
	void simplify_();
};

class Lambda : public Expression {
  public:
	Lambda(const VariableIdentifier& arg, const Application& body, std::string alias = "");
	Lambda(const Lambda &);
	virtual ~Lambda() = default;
	Lambda * clone() const override;
	void printOnStream(std::ostream& os) const override;
	void replace(const VariableIdentifier&, Expression* replacement) override;
	bool tryApply() override;
	Application& apply(Expression* applicant);
	VariableIdentifier arg;
	Application body;
	std::string alias;
};

Lambda * createChurchNumber(unsigned int i);

#include "knownExpressions.h"
#endif //LC_PARSER_H
